package springboot.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Game entity.
 *
 * @author Lisitsyn Roman
 * @version 1.0
 */

@Entity
@Table(name = "games")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

//    @Column(name = "owner")
//    private User owner;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Attempt> attempts = new HashSet<Attempt>();

}
