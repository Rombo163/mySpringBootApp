package springboot.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import springboot.domain.Role;

public interface RoleDao extends JpaRepository<Role, Long> {
}
