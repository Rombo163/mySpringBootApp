package springboot.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import springboot.domain.User;

public interface UserDao extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
