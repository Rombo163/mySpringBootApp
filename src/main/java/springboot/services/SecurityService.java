package springboot.services;

/**
 * SecurityService.
 *
 * @author Lisitsyn Roman
 * @version 1.0
 */
public interface SecurityService {

    String findLoggedInUserName();

    void autoLogin(String username, String password);
}
