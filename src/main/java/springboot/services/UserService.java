package springboot.services;

import springboot.domain.User;

/**
 * Service class for {@link springboot.domain.User}.
 *
 * @author Lisitsyn Roman
 * @version 1.0
 */
public interface UserService {

    void save(User user);

    User findByUsername(String username);
}
