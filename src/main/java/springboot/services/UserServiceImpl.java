package springboot.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import springboot.domain.Role;
import springboot.domain.User;
import springboot.persistence.RoleDao;
import springboot.persistence.UserDao;

import java.util.HashSet;
import java.util.Set;

/**
 * Implemetation of UserService interface.
 *
 * @author Lisitsyn Roman
 * @version 1.0
 */

@Service
public class UserServiceImpl  implements UserService{

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.getOne(1L)); //исправить
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername((username));
    }
}
