<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log in with your account</title>

    <link href="${contextPath}/resources/css/material.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.lime-amber.min.css">
</head>

<body style="height: 100%;">
    <div class="mdl-grid" style="height: 100%;">
        <div class="mdl-cell mdl-cell--12-col mdl-cell--middle" style="width:300px; margin:auto;">
            <div class="mdl-card mdl-card mdl-shadow--4dp">
                <div class="mdl-card__title">
                    <h2 class="mdl-card__title-text">Log in</h2>
                </div>
                <%--<div class="mdl-card__media">--%>
                <%--<img src="photo.jpg" width="220" height="140" border="0" alt="" style="padding:20px;">--%>
                <%--</div>--%>
                <div class="mdl-card__supporting-text">
                    <form method="POST" action="${contextPath}/login" id="loginform">
                        <span>${message}</span>
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="username" name="username">
                            <label class="mdl-textfield__label" for="username">Username</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="password" id="password" name="password">
                            <label class="mdl-textfield__label" for="password">Password</label>
                        </div>
                        <span>${error}</span>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <button form="loginform" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" type="submit">
                        Log In
                    </button>
                </div>
            </div>
        </div>
    </div>
<script src="${contextPath}/resources/js/material.min.js"></script>
</body>
</html>